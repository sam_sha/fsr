import os
import configparser
import pymysql

pymysql.version_info = (1, 4, 13, "final", 0)
pymysql.install_as_MySQLdb()
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

TMP_DIR = os.path.join(BASE_DIR, 'cmdb/tmp')

if not os.path.isdir(TMP_DIR):
    os.makedirs(TMP_DIR)

#get config
config = configparser.ConfigParser()
config.read(os.path.join(BASE_DIR, 'conf/db.conf'))
mysql_db = config.get('mysql_config','db')
mysql_host = config.get('mysql_config','host')
mysql_user = config.get('mysql_config','user')
mysql_passwd = config.get('mysql_config','passwd')
mysql_port = config.get('mysql_config','port')
mysql_charset = config.get('mysql_config','charset')
mysql_timeout = int(config.get('mysql_config','timeout'))


SECRET_KEY='#k)sbqd+f!^3xg0test!a$gt^+&d*1hao*kc(446c)1-rsba3'
# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']

#创建普通用户激活地址(项目地址)
HOST_URL = 'http://10.16.5.155:12000'
# Application definition

INSTALLED_APPS = [
    #'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'dashboard.apps.DashboardConfig',
    'users.apps.UsersConfig',
    'users.templatetags.group_tag',
    'resources.apps.ResourcesConfig',
    'resources.templatetags.serveruser_tag',
    'products.apps.ProductsConfig',
    'devops',
    'confs',
    'django_apscheduler',
    'alert',
    'dbmonitor',
    'django_webssh',
    'channels'
]



MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    #'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]
ASGI_APPLICATION = 'syscmdb.routing.application'
ROOT_URLCONF = 'syscmdb.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')]
        ,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'syscmdb.wsgi.application'


# Database


# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3',
#         'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
#     }
# }

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': mysql_db,
        'USER': mysql_user,
        'PASSWORD': mysql_passwd,
        'HOST': mysql_host,
        'PORT': mysql_port,
        'charset': mysql_charset,
        'timeout': mysql_timeout,
    }
}

# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/

LANGUAGE_CODE = 'zh-Hans'

TIME_ZONE = 'Asia/Shanghai'

USE_I18N = True

USE_L10N = True

USE_TZ = False

LOGIN_URL = '/users/login/'

MAIL_USE_SSL = True
# SESSION配置
SESSION_COOKIE_AGE = 50000000
SESSION_EXPIRE_AT_BROWSER_CLOSE = True

#静态资源加载路径
STATIC_URL = '/static/'
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'cmdb/static'),
]
