# Generated by Django 2.0.6 on 2021-12-01 13:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('devops', '0037_auto_20211130_1530'),
    ]

    operations = [
        migrations.AddField(
            model_name='monitorconfig',
            name='process_name',
            field=models.CharField(blank=True, max_length=200, null=True, verbose_name='进程名称'),
        ),
        migrations.AlterField(
            model_name='monitorconfig',
            name='type',
            field=models.IntegerField(blank=True, choices=[(0, '站点检测'), (1, '端口检测'), (2, '进程检测')], null=True, verbose_name='类型'),
        ),
    ]
