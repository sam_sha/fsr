# _*_ coding: utf-8 _*_
__author__ = 'HaoChenXiao'
from django.conf.urls import url, include
from dbmonitor import views

urlpatterns = [
	url(r'^mysql_dblist/$', views.MySQLListView.as_view(), name = 'mysql_dblist'),
	url(r'^mysqldb_edit/$', views.MySQLDBEditView.as_view(), name = 'mysqldb_edit'),
	url(r'^mysqldb_start/$', views.MySQLDBStartView.as_view(), name = 'mysqldb_start'),
	url(r'^mysqldb_stop/$', views.MySQLDBStopView.as_view(), name = 'mysqldb_stop'),
	url(r'^mysqldb_delete/$', views.MySQLDBDeleteView.as_view(), name = 'mysqldb_delete'),
	url(r'^mysql_detail/$', views.MySQLDBDetailView.as_view(), name = 'mysql_detail'),
	url(r'^mysql_slowlog/$', views.MySQLSlowLogView.as_view(), name = 'mysql_slowlog'),
	url(r'^mysql_slowlog_detail/$', views.MySQLSlowLogDetailView.as_view(), name = 'mysql_slowlog_detail'),
	url(r'^oracle_dblist/$', views.OracleListView.as_view(), name = 'oracle_dblist'),
	url(r'^oracledb_start/$', views.OracleDBStartView.as_view(), name='oracledb_start'),
	url(r'^oracledb_stop/$', views.OracleDBStopView.as_view(), name='oracledb_stop'),
	url(r'^oracledb_delete/$', views.OracleDBDeleteView.as_view(), name='oracledb_delete'),
	url(r'^oracle_tablespace/$', views.OracleTableSpaceView.as_view(), name='oracle_tablespace'),
	url(r'^oracle_detail/$', views.OracleDetailView.as_view(), name='oracle_detail'),




]